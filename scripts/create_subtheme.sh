#!/bin/bash
# This script create sub-theme.

echo "
+------------------------------------------------------+
| With this script you could quickly create sub-theme. |
+------------------------------------------------------+
"
echo "The machine name of your custom theme? [e.g. my_subtheme]"
read -r NEW_ID_NAME
echo "Your theme name ? [e.g. My custom theme]"
read -r NEW_NAME

echo "New machine name:$NEW_ID_NAME"
echo "New theme name:$NEW_NAME"

(
  # Set script directory.
  DIR="$(cd "$(dirname "$0")" && pwd)"
  # Create folder if not exist.
  if [[ ! -e "$DIR/../../../custom" ]]; then
      mkdir "$DIR/../../../custom"
  fi
  cd "$DIR/../../../custom"
  cp -r "../contrib/basic/STARTER" ./$NEW_ID_NAME
  cd "$NEW_ID_NAME"
  # Rename files.
  for file in *STARTER.*
  do
    mv "$file" "${file//STARTER/$NEW_ID_NAME}";
  done
  # Set new theme name.
  sed -i "s/STARTER_THEME_NAME/$NEW_NAME/" "$NEW_ID_NAME.info.yml"
  # Correct library name.
  sed -i "s/STARTER_THEME_ID/$NEW_ID_NAME/" "$NEW_ID_NAME.info.yml"
  # Remove "hidden" property.
  sed -i  "/# Remove this line/,/hidden:/d" "$NEW_ID_NAME.info.yml"

  echo "# Created your new sub-theme."
) || (
  echo "Script execution error. Exit"
)
