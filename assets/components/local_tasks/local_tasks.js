/**
 * @file
 * Defines event for local_task submit.
 */

(function () {

  let localTasks = {
    attach: function () {
      var elem = document.querySelector('.local_tasks_submit');
      var self = this;
      if (elem) {
        if (elem.classList.contains('once')) {
          return;
        }
        elem.addEventListener('click', function () {
          if (!self.hasOwnProperty('block')) {
            self.block = document.querySelector('.local_tasks');
          }
          self.block.classList.toggle('show');
        }, false);
        elem.classList.add('once');
      }
    }
  };

  if (typeof Drupal !== 'undefined') {
    Drupal.behaviors.localTasks = localTasks;
  }
  else {
    localTasks.attach();
  }
}());
